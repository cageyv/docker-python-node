# docker-python-node

[![pipeline status](https://gitlab.com/guardianproject-ops/docker-python-node/badges/master/pipeline.svg)](https://gitlab.com/guardianproject-ops/docker-python-node/-/commits/master)

Docker image containing Python and Node.js

* os: debian slim buster base
* python: 3.7 or 3.8
* node: 12 or 13
* yarn: stable
* pip: latest
* pipenv: latest

### Available tags:

```
python3.7-nodejs12
python3.7-nodejs13
python3.8-nodejs12
python3.8-nodejs13
```

## License

[![License GNU AGPL v3.0](https://img.shields.io/badge/License-AGPL%203.0-lightgrey.svg)](https://gitlab.com/digiresilience/waterbear/zamamd-addon-waterbear/blob/master/LICENSE.md)

This is a free software project licensed under the GNU Affero General
Public License v3.0 (GNU AGPLv3) by [The Center for Digital
Resilience](https://digiresilience.org) and [Guardian
Project](https://guardianproject.info).

