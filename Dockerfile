ARG PY_IMAGE=python:3.7-slim-buster
FROM $PY_IMAGE
LABEL maintainer="Abel Luck <abel@guardianproject.info>"

RUN groupadd --gid 1000 node \
  && useradd --uid 1000 --gid node --shell /bin/bash --create-home node

ARG NODE_VERSION=12
ARG NPM_VERSION=6
RUN set -ex ; \
  apt-get update ; \
  apt-get  install -y gnupg2 wget; \
  echo "deb https://deb.nodesource.com/node_$NODE_VERSION.x buster main" > /etc/apt/sources.list.d/nodesource.list ; \
  wget -qO- https://deb.nodesource.com/gpgkey/nodesource.gpg.key | apt-key add - ; \
  echo "deb https://dl.yarnpkg.com/debian/ stable main" > /etc/apt/sources.list.d/yarn.list ; \
  wget -qO- https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add - ; \
  apt-get update ; \
  apt-get install -yqq nodejs=$(apt-cache show nodejs|grep Version|grep nodesource|cut -c 10-) yarn ; \
  apt-mark hold nodejs ; \
  pip install -U pip && pip install pipenv ; \
  npm i -g npm@^$NPM_VERSION ; \
  rm -rf /var/lib/apt/lists/*
