DOCKER_NS ?= guardianproject-ops/docker-python-node
TAGS := 3.7/12 3.8/12 3.7/13 3.8/13

build: $(TAGS)

$(TAGS):
	docker build --pull --no-cache --build-arg PY_IMAGE=python:$(@D)-slim-buster --build-arg NODE_VERSION=$(@F) -t ${DOCKER_NS}:python$(@D)-nodejs$(@F) ${PWD}
	docker push ${DOCKER_NS}:python$(@D)-nodejs$(@F)
